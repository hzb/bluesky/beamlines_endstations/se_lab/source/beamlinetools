from secop_ophyd.SECoPDevices import SECoPCMDDevice, SECoPReadableDevice, SECoPMoveableDevice, SECoPNodeDevice 
from ophyd_async.core.signal import SignalR, SignalRW 


class MassflowController(SECoPMoveableDevice):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    target: SignalRW
    ramp: SignalRW
    gastype: SignalR
    tolerance: SignalR
    stop_CMD: SECoPCMDDevice
    test_cmd_CMD: SECoPCMDDevice


class PressureController(SECoPMoveableDevice):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    target: SignalRW
    ramp: SignalRW
    tolerance: SignalR
    stop_CMD: SECoPCMDDevice


class TemperatureController(SECoPMoveableDevice):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    target: SignalRW
    ramp: SignalRW
    tolerance: SignalR
    stop_CMD: SECoPCMDDevice


class TemperatureSensor(SECoPReadableDevice):
    status: SignalR
    group: SignalR
    description: SignalR
    implementation: SignalR
    interface_classes: SignalR
    features: SignalR
    value: SignalR
    heat_flux: SignalRW
    stop_CMD: SECoPCMDDevice


class Gas_dosing(SECoPNodeDevice):
    equipment_id: SignalR
    firmware: SignalR
    version: SignalR
    description: SignalR
    interface: SignalR
    massflow_contr1: MassflowController
    massflow_contr2: MassflowController
    massflow_contr3: MassflowController
    backpressure_contr1: PressureController


class Reactor_cell(SECoPNodeDevice):
    equipment_id: SignalR
    firmware: SignalR
    version: SignalR
    description: SignalR
    interface: SignalR
    temperature_reg: TemperatureController
    temperature_sam: TemperatureSensor


