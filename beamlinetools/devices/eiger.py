from ophyd import AreaDetector, SingleTrigger, EigerDetector

class MyDetector(EigerDetector):
    pass

    def set_aquisition_time(self, value):
        """The aquire time is in seconds"""
        self.cam.acquire_time.set(value)
        self.cam.acquire_period.set(value)
        

    def stage(self):
        self.cam.fw_name_pattern.set("user/mySpot/2024-03-12_test/test") # EIGER_dir_name = ME[1]  # EIGER_file_name = ME[2]
        self.cam.trigger_mode.set(3) # this set it to internal series, EIGER_trigger_mode = ME[4]
        self.cam.num_triggers.set(3) # This is now the number of images per file
        self.cam.num_images.set(1) # EIGER_nimages = int(ME[5])
        self.cam.fw_num_images_per_file.set(1) 
        self.cam.photon_energy.set(8000) # we have to catch this automatically
        self.cam.compression_algo.set(1) # set compression method to bslz4
        self.cam.beam_center_y.set(1500)
        self.cam.det_distance.set(300)
        self.cam.acquire_time.set(1)
        self.cam.acquire.set(1) # arm the detector, the detector is now waiting to be triggered
        # I could not find this, not sure is needed. EIGER_image_start_nr = int(ME[3])
        super().stage()

    def unstage(self):
        self.cam.acquire.set(0) # disarm the detector
        super().unstage()

    def trigger_to_be_written(self):
        #Create a callback called if count is processed
        def new_value(*,old_value,value,**kwargs):          #MDEL of $(P):rdCur must be set to -1

            status.set_finished()
            
            # Clear the subscription.
            self.readback.clear_sub(new_value)

        #Create the status object
        status = DeviceStatus(self.readback,timeout = 10.0)

        #Connect the callback that will set finished and clear sub
        self.readback.subscribe(new_value,event_type=Signal.SUB_VALUE,run=False)
        

        self.cam.special_trigger_button.set(1)  
        
        return status



	    
# - We need to send the parameters and arm the detector
# - we need to send a software trigger at every measurements
# - we have to be able to cancel and abort the scan
#  - we disarm the detector at the end
# - we have to be able to wait for the detector to end the acquisition or whatever it is doing. 