import numpy as np


from ophyd.mca import ROI

from ophyd import EpicsSignal, EpicsSignalRO
from ophyd.status import SubscriptionStatus, AndStatus 
from ophyd.device import DynamicDeviceComponent as DDC
from ophyd.device import Device, Component as Cpt
from ophyd import FormattedComponent as FCpt

from ophyd.signal import DerivedSignal 


class DivideByArgs(DerivedSignal):
    def __init__(self, derived_from, divisors, log=False,  write_access=None, name=None, parent=None, **kwargs):
        if divisors.__class__ is not list:
            raise ValueError(f"divisors should be a list, while it is a {type(divisors)}") 
        if len(divisors) == 0:
            raise ValueError(f"The divisors list should contain at least one element")
        self._divisors = divisors
        
        if log.__class__ is not bool:
            raise ValueError(f"log should be a bool, while it is a {type(log)}") 
        self._log = log
        super().__init__(derived_from, write_access=None, name=None, parent=None, **kwargs)

    def inverse(self, value):
        """Compute original signal value -> derived signal value"""
        divisor = 1
        for div in self._divisors:
            divisor *= div.get()
        value = value / divisor
        if self._log:
            value = np.log(value)
        return value

    def forward(self, value):
        """Compute derived signal value -> original signal value"""
        return value

    
class ROI(Device):

    # 'name' is not an allowed attribute
    label = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}NM', lazy=True, kind='config')
    count = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}', lazy=True, kind='normal')
    net_count = FCpt(EpicsSignalRO, '{self.prefix}.R{self._ch}N', lazy=True, kind='config')
    preset_count = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}P', lazy=True, kind='config')
    is_preset = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}IP', lazy=True, kind='config')
    bkgnd_chans = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}BG', lazy=True, kind='config')
    # hi_chan = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}HI', lazy=True, kind='config')
    # lo_chan = FCpt(EpicsSignal, '{self.prefix}.R{self._ch}LO', lazy=True, kind='config')
    # hi_en = FCpt(EpicsSignal, '{self.prefix}:R{self._ch}HIENERGY', lazy=True, kind='config')
    # lo_en = FCpt(EpicsSignal, '{self.prefix}:R{self._ch}LOENERGY', lazy =True, kind='config')            


    
    def __init__(self, prefix,ch, *, read_attrs=None, configuration_attrs=None,
                 name=None, parent=None, **kwargs):
        super().__init__(prefix, read_attrs=read_attrs,
                         configuration_attrs=configuration_attrs,
                         name=name, parent=parent, **kwargs)
        self._ch = ch
        self.hide(verbose=False)
        

    
    def display(self, verbose=True):

        self.count.kind="hinted"        
        if verbose:
            dev_name = self.name
            dev_name = dev_name.replace("_", ".")
            print(f"{dev_name} will be plotted")
    
    def hide(self, verbose=True):

        self.count.kind="normal"
        if verbose:
            dev_name = self.name
            dev_name = dev_name.replace("_", ".")
            print(f"{dev_name} will not be plotted")
    
class Channel(Device):
    # ROIS
    roi0 =Cpt(ROI, '', ch=0, kind='normal')
    roi1 =Cpt(ROI, '', ch=1, kind='normal')
    roi2 =Cpt(ROI, '', ch=2, kind='normal')
    roi3 =Cpt(ROI, '', ch=3, kind='normal')
    roi4 =Cpt(ROI, '', ch=4, kind='normal')
    roi5 =Cpt(ROI, '', ch=5, kind='normal')
    roi6 =Cpt(ROI, '', ch=6, kind='normal')    
    roi7 =Cpt(ROI, '', ch=7, kind='normal')
 
    #calibration
    offset = Cpt(EpicsSignalRO, '.CALO',kind='config')
    slope = Cpt(EpicsSignalRO, '.CALS',kind='config')
    quadratic = Cpt(EpicsSignalRO, '.CALQ',kind='config')
    egu = Cpt(EpicsSignalRO, '.EGU',kind='config')
    two_theta = Cpt(EpicsSignalRO, '.TTH',kind='config')

    # lifetime
    lifetime = Cpt(EpicsSignalRO, '.ELTM', kind='config')
    realtime = Cpt(EpicsSignalRO, '.ERTM', kind='config')
    deadtime = Cpt(EpicsSignalRO, '.IDTIM', kind='config')

    def __init__(self, prefix, *, kth=None,read_attrs=None, configuration_attrs=None,
                 name=None, parent=None, **kwargs):
        super().__init__(prefix, read_attrs=read_attrs,
                         configuration_attrs=configuration_attrs,
                         name=name, parent=parent, **kwargs)


    

class MyEpicsMCA(Device):
    
    # Aquisition
    start_all   = Cpt(EpicsSignal, "StartAll")
    stop_all    = Cpt(EpicsSignal, "StopAll")
    erase_start = Cpt(EpicsSignal, "EraseStart")
    erase       = Cpt(EpicsSignal, "EraseAll")
    acquiring   = Cpt(EpicsSignal, "Acquiring")
    done_value  = 0
    
    # Preset info
    preset_events = Cpt(EpicsSignal, "PresetEvents")
    preset_live_time = Cpt(EpicsSignal, "PresetLive")
    preset_real_time = Cpt(EpicsSignal, "PresetReal")
    preset_mode = Cpt(EpicsSignal, "PresetMode", string=True)
    preset_triggers = Cpt(EpicsSignal, "PresetTriggers")

    # channels
    ch1 = Cpt(Channel, "mca1")
    ch2 = Cpt(Channel, "mca2")
    ch3 = Cpt(Channel, "mca3")

    
    aquiring_time = 4

    def stage(self):
        self.old_aquiring_time=self.preset_real_time.get()
        self.preset_real_time.set(self.aquiring_time)

    def unstage(self):
        self.preset_real_time.set(self.old_aquiring_time)

    def trigger(self):
        
        callback_signal = self.acquiring
        #variable used as an event flag
        acquisition_status = False
           
        def acquisition_started(status):
            nonlocal acquisition_status #Define as nonlocal as we want to modify it
            acquisition_status = True
                
        def check_value(*, old_value, value, **kwargs):
            #Return True when the acquisition is complete, False otherwise.
                                   
            if not acquisition_status:  #But only report done if acquisition was already started
                
                return False
                       
            return (value == self.done_value)
        
        # create the status with SubscriptionStatus that add's a callback to check_value.
        sta_cnt = SubscriptionStatus(callback_signal, check_value, run=False)
         
        # Start the acquisition        print(f"init {name}")

        sta_acq = self.erase_start.set(1)
        
        sta_acq.add_callback(acquisition_started)
        
        stat = AndStatus(sta_cnt, sta_acq)
        
        return stat
    


    

