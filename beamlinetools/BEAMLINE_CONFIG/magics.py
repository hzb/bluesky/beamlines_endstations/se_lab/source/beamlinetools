from beamlinetools.magics.simplify_syntax import Simplify
from beamlinetools.magics.peakinfo import PeakInfoMagic
from bluesky.magics import BlueskyMagics
from beamlinetools.magics.standard_magics import BlueskyMagicsBessy
from IPython import get_ipython
from .base import RE, bec

## IMPORTANT : do not change the order of the follwing two lines. 
# standard magics
get_ipython().register_magics(BlueskyMagics)
# custom magics - it will override some standard magics
label_axis_dict = {
"temperature_regulator": ["reactor_c.temperature_reg.target", 
              "reactor_c.temperature_reg.value",
              "reactor_c.temperature_reg.ramp",
              "reactor_c.temperature_reg.status",
            ],
"temperature_sensor": ["reactor_c.temperature_sam.value",
              "reactor_c.temperature_sam.status",
            ],
"pressure_controller_1": ["gas_d.backpressure_contr1.target", 
              "gas_d.backpressure_contr1.value",
              "gas_d.backpressure_contr1.ramp",
              "gas_d.backpressure_contr1.status",
            ]
}
exclude_labels_from_wa=['detectors', 'motors']
get_ipython().register_magics(BlueskyMagicsBessy(RE, 
                                                 get_ipython(), 
                                                 database_name ="db", 
                                                 exclude_labels_from_wa=exclude_labels_from_wa,
                                                 label_axis_dict=label_axis_dict))

simplify = Simplify(get_ipython())
simplify.autogenerate_magics('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')
run_plan = simplify.execute_magic

get_ipython().register_magics(PeakInfoMagic)
# usage: peakinfo