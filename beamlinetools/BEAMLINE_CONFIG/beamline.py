# Workarounds/patches
from ophyd.signal import EpicsSignalBase
EpicsSignalBase.set_defaults(connection_timeout= None, timeout=200)

from bessyii_devices.ring import Ring

from beamlinetools.devices.ophydMFLibrary import keithley6485

from beamlinetools.devices.au import AUmySpot


from beamlinetools.devices.dcm_mySpot import DCMmySpot
from ophyd import EpicsMotor


# standard magics
from bluesky.magics import BlueskyMagics
get_ipython().register_magics(BlueskyMagics)

# If magics for the beamline are defined import them like this
# (example taken from AQUARIUS beamline)
#from aquariustools.plans.magics_aquarius import BlueskyMagicsAQUARIUS
#get_ipython().register_magics(BlueskyMagicsAQUARIUS)

# simulated devices
from ophyd.sim import det1, det2, det3, det4, motor1, motor2, motor, noisy_det   # two simulated detectors

### SECoP-Ophyd Device ###

from .base import RE
from ophyd_async.core import DeviceCollector
from secop_ophyd.SECoPDevices import SECoPNodeDevice
import asyncio
from beamlinetools.devices.genNodeClass import Reactor_cell, Gas_dosing
from pprint import pprint

# relative path to directory where generated Module should be stored
#genCodeDir = "/opt/bluesky/beamlinetools/devices"

#  # Connect to Gas Dosing SEC Node and generate ophyd device trees
gas_d = SECoPNodeDevice.create('localhost','10800',RE.loop)

# # Connect to Reactor Cell SEC Node and generate ophyd device tree
reactor_c = SECoPNodeDevice.create('localhost','10801',RE.loop)

gas_d.class_from_instance()
reactor_c.class_from_instance()

reactor_c:Reactor_cell = reactor_c 
gas_d:Gas_dosing = gas_d
