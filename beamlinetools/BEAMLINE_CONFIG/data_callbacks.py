
from .base import *
from datetime import datetime
from beamlinetools.callbacks.mca_callback import McaCallback
from beamlinetools.callbacks.file_exporter import CSVCallback, SpecWriterCallback
from apstools.callbacks import NXWriter
import pathlib
import os

# Nexus Callback
# h5_file = pathlib.Path("/opt/bluesky/data")

# nxwriter = NXWriter()
# RE.subscribe(nxwriter.receiver)
# nxwriter.file_name = str(h5_file)
# nxwriter.warn_on_missing_content = False

# SPEC callback
year = str(datetime.now().year)

specwriter = SpecWriterCallback(filename="/opt/bluesky/data/beamline"+year+".spec")
RE.subscribe(specwriter.receiver)

# data manager, export scans to single csv file

dm = CSVCallback(file_path="/opt/bluesky/data")
dm.change_user("beamline_commissioning_"+year)
changeuser=dm.change_user
RE.subscribe(dm.receiver)

# MCA Callback
RE.subscribe(McaCallback())
