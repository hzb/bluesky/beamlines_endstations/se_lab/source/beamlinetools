from bluesky.preprocessors import SupplementalData
from bessyii.default_detectors import SupplementalDataSilentDets, init_silent, close_silent
from .base import *
from .beamline import *



# here the devices that will be read before and after a scan
baseline = [det1]

# here the devices to be read during each scan, 
# but not plotted unless explicitly set in the plan
silent_devices = []#[kth01, kth00]

sd = SupplementalDataSilentDets(baseline=baseline, silent_devices=silent_devices)

#add the functions to the RunEngine library so you can call them via Msg
RE.register_command('init_silent', init_silent)
RE.register_command('close_silent', close_silent)

RE.preprocessors.append(sd)